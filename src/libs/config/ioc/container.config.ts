import 'reflect-metadata';
import { Container } from 'inversify';
import { ProductService } from '../../../services/product.service';
import { Types } from './types';
import { ProductController } from '../../../controllers/product.controller';

const container = new Container();

container.bind<ProductService>(Types.ProductService).to(ProductService).inSingletonScope()

container.bind<ProductController>(Types.ProductController).to(ProductController).inSingletonScope()
export default container;
