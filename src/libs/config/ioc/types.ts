import { ProductService } from '../../../services/product.service';
import { ProductController } from '../../../controllers/product.controller';

export const Types = {
    // Services
    ProductService: Symbol.for('ProductService'),

    // Controllers
    ProductController: Symbol.for('ProductController')
};