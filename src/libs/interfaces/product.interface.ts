export interface IProductService {
    getProducts(): Promise<Record<string, any>[]>;
}