import express from 'express';
import 'express-async-errors';
import 'dotenv/config';
import container from './libs/config/ioc/container.config';
import { InversifyExpressServer } from 'inversify-express-utils';
import { NotFoundError } from './libs/errors/not-found.error';
import { errorHandler } from './libs/middlewares/error-handler.middleware';


const server = new InversifyExpressServer(container);

const app = server.build();
app.use(express.json());

app.all('*', async () => {
    throw new NotFoundError();
});

app.use(errorHandler);
export { app };