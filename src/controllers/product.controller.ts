import { inject } from 'inversify';
import { Types } from '../libs/config/ioc/types';
import { ProductService } from '../services/product.service';
import { controller, httpGet } from "inversify-express-utils";

@controller('/api/products')
export class ProductController {
    constructor(@inject(Types.ProductService) private readonly productService: ProductService) {}

    @httpGet('')
    getProducts(): Promise<Record<string, any>[]> {
        return this.productService.getProducts();
    }

}